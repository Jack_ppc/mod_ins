import os
import tkinter as tk
from tkinter import ttk
from subprocess import call
import re

def go():
    if v_xvm.get():

        def clean_dir(_dir):
            for i in os.listdir(_dir):
                if i != g_ver and re.search(r'^\d[\.\d+]+\d+$', i):
                    call(['cmd', '/c', 'rmdir', '/s', '/q', _dir + '\\' + i])

        clean_dir(to_WOT)
        clean_dir(to_WOT[:-4] + 'res_mods')
        call(['del_xvm.bat', g_ver])
    for mod in mods:
        if mod['check'].get():
            fullname = desk_dir + mod['archive']
            call([arch, 'x', fullname, '-o' + fullname[:-4]])
            if mod['name'] == 'Session statistics':
                mod['path'] = '\\' + mod['archive'][:-4] + r'\mods'
            call(['xcopy', fullname[:-4] + mod['path'], to_WOT, '/e', '/y'])
            if mod['name'] == 'White corpses':
                call(['xcopy', fullname[:-4] + r'\Белые трупы\mods', to_WOT, '/e', '/y'])
            if mod['name'] == 'Jimbo\'s sight':
                call(['xcopy', fullname[:-4] + r'\Сведение 2', to_WOT + '\\' + g_ver, '/e', '/y'])
            if mod['name'] == 'XVM':
                call(['_XVM_.bat', fullname[:-4], to_WOT[:-4] + 'res_mods'])
            if mod['name'] != 'XVM':
                call(['removing.bat', fullname[:-4]])
    if v_TIM.get():
        call(['_TIM.bat', g_ver])
    root.destroy() 
# ------------------------------------------------
mods = [
    {
        'name': 'Battle hits',
        'check': None,
        'archive': '',
        'pattern': '805-poliroid',
        'path': r'\mods'
        },
    {
        'name': 'Armor analysis',
        'check': None,
        'archive': '',
        'pattern': 'mod-dlya-analiza-broni-tankov',
        'path': r'\mods'
        },
    {
        'name': 'Marks on the gun',
        'check': None,
        'archive': '',
        'pattern': 'mod-stvol',
        'path': r'\Вариант 2\mods'
        },
    {
        'name': 'Session statistics',
        'check': None,
        'archive': '',
        'pattern': 'Wotstat_Slava7572',
        'path': ''
        },
    {
        'name': 'White corpses',
        'check': None,
        'archive': '',
        'pattern': 'mod-belye-trupy-tankov',
        'path': r'\Белые сбитые гусеницы\mods'
        },
    {
        'name': 'Jimbo\'s sight',
        'check': None,
        'archive': '',
        'pattern': 'pritsely-ot-jimbo',
        'path': r'\mods'
        },
    {
        'name': 'XVM',
        'check': None,
        'archive': '',
        'pattern': r'.*xvm.*zip',
        'path': r'\mods'
        }
    ]            

desk_dir = r'C:\Users\WIN10_GPT\Desktop\\'                  # path to Desktop
desk = os.listdir(desk_dir)                                 # Desktop files
arch = r'C:\Program Files\7-Zip\7z'                         # path to 7z.exe
to_WOT = r'D:\Games\World_of_Tanks\mods'                    # mods Directory
my_mods_dir = r'D:\Games\Mods for WoT\\'                    # path to my mods
with open(to_WOT[:-4] + 'version.xml') as px:               # WOT version
    for _ in range(3):
        g_ver = px.readline()[13:-18]

root = tk.Tk()
root.title(f'Installing some mods for WOT!')
x = root.winfo_screenwidth() // 2 - 350
y = root.winfo_screenheight() // 2 - 250
root.geometry(f'700x400+{x}+{y}')

f_left = ttk.LabelFrame(text='XVM')
f_right = ttk.LabelFrame(text='mods for installing')
f_left.place(relwidth=.5, relheight=1)
f_right.place(x=350, relwidth=.5, relheight=1)

ttk.Style().configure('.', font=("TkDefaultFont", 12))
ttk.Style().configure('TLabel', foreground='lightgrey')

for mod in mods:
    var = tk.BooleanVar()
    for file in desk:
        if re.search(mod['pattern'], file):
            mod['archive'] = file
            var.set(1)
            ttk.Checkbutton(f_right, text=mod['name'], variable=var).pack(anchor='w', padx=100, pady=10)
            break
    if not mod['archive']:
        ttk.Label(f_right, text=mod['name']).pack(anchor='w', padx=120, pady=10)
    mod['check'] = var
v_xvm = tk.BooleanVar()
ttk.Checkbutton(f_left, text='delete prev XVM', variable=v_xvm).pack(anchor='w', padx=100, pady=30)
v_TIM = tk.BooleanVar()
ttk.Checkbutton(f_left, text='update tank icons', variable=v_TIM).pack(anchor='w', padx=100, pady=30)

ttk.Button(text='GO!', command=go).place(x=300, y=350)

root.mainloop()
