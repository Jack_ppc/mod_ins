@echo off
set wot=D:\Games\World_of_Tanks\
set icons="D:\Games\Mods for WoT\XVM\important trash\tank_icons"
robocopy %icons% %wot%res_mods\%1\gui\flash\atlases Ally.* Enemy.*
rmdir /q /s %wot%mods\%1\com.modxvm.xfw %wot%mods\temp
rmdir /s /q %wot%res_mods\%1\audioww
rmdir /s /q %wot%res_mods\configs\xvm\default %wot%res_mods\configs\xvm\sirmax
del %wot%res_mods\configs\xvm\configs.url %wot%res_mods\configs\xvm\xvm.xc.sample
rmdir /s /q %wot%res_mods\configs\xvm\py_macro\xvm
cd /d %wot%res_mods\configs\xvm\py_macro
attrib +r battleMessages.py
attrib +r damage_indicator.py
attrib +r sixthSense.py
del /q *
cd  %wot%res_mods\mods\xfw_packages
move /y xvm_sounds_addons ..\xvm_sounds_addons
rmdir /s /q %wot%res_mods\mods\xfw_packages
move /y ..\xvm_sounds_addons xvm_sounds_addons
cd %wot%res_mods\mods\shared_resources\xvm
rmdir /s /q doc l10n
attrib +r audioww\SM_*
del /q audioww\*
rmdir /s /q res\clanicons res\locker res\icons
del /q res\MinimapAim.png res\SixthSense.png.sample
